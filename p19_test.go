package p19

import (
	"context"
	"net"
	"os"
	"os/signal"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
)

func TestP19Fit(t *testing.T) {
	tt := base.Test{t}
	fits := map[int]Fit {
		 0: FitOk0,
		 1: FitOkMore1,
		 2: FitOk0,
		 3: FitNo2,
		 4: FitOkPartial3,
		 5: FitNo2,
		 6: FitNoPartial5,
		 7: FitOkPartial3,
		 8: FitNo2,
		 9: FitNoPartial5,
		10: FitOkPartialMore4,
		11:	FitNo2,
		12:	FitOkPartial3,
		13: FitNo2,
		14: FitNoPartial5,
		15: FitNoPartial5,
		16: FitNoPartial5,
		17: FitNoPartial5,
		18: FitNoPartial5,
		19: FitNoPartial5,
		20: FitOkPartial3,
	}
	var nFrames, nHbs, nCans int
	frames := func(p *Frames) {
		if fit, ok := fits[nFrames]; ok {
			tt.Equals(fit, p.Fit)
		}
	}
	req := NewFrameReqs()
	go func() {
		for {
			select {
			case req := <-req.Req():
				if f := req.Frames; f != nil {
					frames(f)
					nFrames++
				}
				if can := req.Can; can != nil {
					nCans++
				}
				if req.Heartbeat {
					nHbs++
				}
			}
		}
	}()

	reader := NewReader(req)
	complete := []byte{
		0xAA, 0x08, 0x00, // size=8
		1, 2, 3, 4, 5, 6, 7, 8,
	}
	// Single complete
	reader.Read(complete) // p=0 expect FitOk0
	
	// Joining two completes
	a := append(complete, complete...)
	reader.Read(a) // p=1 expect FitOkMore1
	
	// p=2 expect FitOk0
	// Single complete in two parts
	b1, b2 := complete[:6], complete[6:]
	reader.Read(b1) // p=3 expect FitNo2
	reader.Read(b2) // p=4 expect FitOkPartial3
	// Single complete in three parts:
	reader.Read(complete[:5])  // [170 8 0 1 2] p=5 expect FitNo2
	reader.Read(complete[5:8]) // [3 4 5]       p=6 expect FitNoPartial5
	reader.Read(complete[8:])  // [6 7 8]       p=7 expect FitOkPartial3
	// Double in four parts to get FitOkPartialMore4
	reader.Read(a[:5])    // [170 8 0 1 2]   p= 8 expect FitNo2
	reader.Read(a[5:10])  // [3 4 5 6 7]     p= 9 expect FitNoPartial5
	reader.Read(a[10:15]) // [8 170 8 0 1]   p=10 expect FitOkPartialMore4
	//                 p=11 expect FitNo2
	reader.Read(a[15:]) // [2 3 4 5 6 7 8] p=12 expect FitOkPartial3

	ones := make([][]byte, len(complete))
	for p := range complete {
		ones[p] = []byte{complete[p]}
	}
	// [[170] [8] [0] [1] [2] [3] [4] [5] [6] [7] [8]]
	for _, one := range ones {
		reader.Read(one)
		// 170
		// 8
		// 0
		// 1   p=13 expect FitNo2
		// 2   p=14 expect FitNoPartial5
		// 3   p=15 expect FitNoPartial5
		// 4   p=16 expect FitNoPartial5
		// 5   p=17 expect FitNoPartial5
		// 6   p=18 expect FitNoPartial5
		// 7   p=19 expect FitNoPartial5
		// 8   p=20 expect FitOkPartial3
	}
	time.Sleep(1 * time.Second)
	tt.Equals(21, nFrames)
	tt.Equals(0, nCans)
	tt.Equals(0, nHbs)
}

var T1 = []byte{
	// frame[0] -> 1 can
	0xAA, 21, 0, // size 21 + 0*256
	56, 208, 243, 75, // time0
	1, // one fragment
	0xA5, 14, 5, // fragment 14=size, 5=request-can
	1, 1, 1, 0, 4, 2, 3, 232, 3, 184, 3, 184, 0,
	
	// frame[1] -> 1 can
	0xAA, 21, 0, // size 21 + 0*256
	81, 208, 243, 75, // time
	1, // one fragment
	0xA5, 14, 5, // fragment 14=size, 5=request-can
	1, 1, 1, 0, 4, 3, 140, 132, 0, 0, 0, 0, 0,

	// frame[2] -> 6 cans
	0xAA, byte(5 + 6*20), 0, // 125,0
	1, 0, 0, 0, // 4 = time
	0x06, // follow six 0xA5s
	// 0xA5,18,5 -> fragment size=18 5=can-request
	0xA5, 18, 5, 0x01, 0x6F, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0xF5, 0x08, 0x2D, 0x10, 0x43, 0x0F, 0x41, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xAA, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x3E, 0x10, 0x3B, 0x0F, 0x37, 0x0F, 0x34, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xB8, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x32, 0x0F, 0x2E, 0x0F, 0x2C, 0x0F, 0x2C, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xE9, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x2A, 0x0F, 0x29, 0x10, 0x28, 0x0F, 0x25, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xF3, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x0C, 0x00, 0x02, 0x00, 0x3C, 0x00, 0x62, 0x0A, 0x28, // 20
	0xA5, 18, 5, 0x01, 0xFC, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x0E, 0x01, 0x1A, 0x00, 0x43, 0x00, 0xD9, 0x00, 0x00, // 20

	// frame[3] -> 1 hb
	0xAA,
	byte(5 + 3), 0,
	2, 0, 0, 0, // 4
	0x01,             // 1 one A5s
	0xA5, 1, 0x04, // 0x04=hb

	// frame[4] -> 4 cans
	0xAA,
	byte(5 + 4*20), 0, // 85,0
	3, 0, 0, 0, // 4
	0x04, // follow four A5s
	0xA5, 18, 5, 0x01, 0x58, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x21, 0x0F, 0x1B, 0x10, 0x11, 0x0F, 0x0D, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xB3, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0F, 0x0F, 0x0E, 0x0F, 0x0D, 0x0F, 0x0E, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xC1, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0E, 0x0F, 0x0D, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, // 20
	0xA5, 18, 5, 0x01, 0xCB, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0E, 0x10, 0x0F, 0x0F, 0x0E, 0x0F, 0x0D, 0x0F, // 20
}

func TestP19Reader1(t *testing.T) {
	tt := base.Test{t}
	var nFrames, nHbs, nCans int
	frames := func(p *Frames) {
		switch nFrames {
		case 0:
			tt.Equals(1, len(p.Frames))
			tt.Equals(int64(56 + 208<<8 + 243<<16 + 75<<24), p.Time)
			tt.Equals(Frame([]byte{5,1,1,1,0,4,2,3,232,3,184,3,184,0}), p.Frames[0])
			tt.Ok(p.Err)
		case 1:
			tt.Equals(1, len(p.Frames))
			tt.Equals(int64(81 + 208<<8 + 243<<16 + 75<<24), p.Time)
			tt.Equals(Frame([]byte{5,1,1,1,0,4,3,140,132,0,0,0,0,0}), p.Frames[0])
			tt.Ok(p.Err)
		}
		t.Logf("%sframe[%d]: %v%s", base.Green, nFrames, p, base.Reset)
		nFrames++
	}
	can := func(c *Can) {
		switch nCans {
		case 0:
			tt.Equals(byte(1), c.Net)
			tt.Equals([]byte{1,1,0,4,2,3,232,3,184,3,184,0}, c.Data.Bytes())
		case 1:
			tt.Equals(byte(1), c.Net)
			tt.Equals([]byte{1,1,0,4,3,140,132,0,0,0,0,0}, c.Data.Bytes())
		}
		t.Logf("%scan[%d]: %v%s", base.Blue, nCans, c, base.Reset)
		nCans++
	}
	hb := func() {
		t.Logf("%sheartbeat[%d]%s", base.Yellow, nHbs, base.Reset)
		nHbs++
	}
	reqs := NewFrameReqs()
	go func() {
		for {
			select {
			case req := <-reqs.Req():
				if f := req.Frames; f != nil {
					frames(f)
				} 
				if c := req.Can; c != nil {
					can(c)
				}
				if req.Heartbeat {
					hb()
				}
			}
		}
	}()
	reader := NewReader(reqs)
	reader.Read(T1)
	time.Sleep(1 * time.Second)
	tt.Equals([]int{5,2,0}, []int{nFrames, nCans, nHbs})
}

type c2 struct {
	t *testing.T
	nFrames int
	nHbs    int
	nCans   int
}

func (s *c2) frames(p *Frames) {
	switch s.nFrames {
	case 0:
		if m, is, must := "%slen(payloads[0].Frames) is %d must be %d%s", len(p.Frames), 6; is != must {
			s.t.Fatalf(m, base.Red, is, must, base.Reset)
		}
	case 1:

	case 2:
		if m, is, must := "%slen(payloads[2].Frames) is %d must be %d%s", len(p.Frames), 4; is != must {
			s.t.Fatalf(m, base.Red, is, must, base.Reset)
		}
	}
	s.nFrames++
}

func (s *c2) heartbeat() {
	s.nHbs++
}

func (s *c2) can(resp *Can) {
	switch s.nCans {
	case 9: // last can, the fourth of the third payload
		if m, is, must := "%sc[9] Net is %d must be %d%s", resp.Net, byte(1); is != must {
			s.t.Fatalf(m, base.Red, is, must, base.Reset)
		}
		lastTime := int64(0xCB + 0xFF<<8 + 0xFF<<16 + 0xFF<<24)
		if m, is, must := "%sc[9] Time is %d must be %d%s", resp.Msec, lastTime; is != must {
			s.t.Fatalf(m, base.Red, is, must, base.Reset)
		}
		if m, is, must := "%sc[9] Command is %d must be %d%s", resp.Data.Comm(), byte(4); is != must {
			s.t.Fatalf(m, base.Red, is, must, base.Reset)
		}
	}
	s.nCans++
}

var T23 = []byte{
	// case 0 -> 6 cans
	0xAA,
	byte(5 + 6*20), 0, // 125,0
	1, 0, 0, 0, // 4 = time
	0x06, // follow six 0xA5s
	// 0xA5,0x12,0x03 -> fragment size=18 0x05=can-request
	0xA5, 0x12, 5, 0x01, 0x6F, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0xF5, 0x08, 0x2D, 0x10, 0x43, 0x0F, 0x41, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xAA, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x3E, 0x10, 0x3B, 0x0F, 0x37, 0x0F, 0x34, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xB8, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x32, 0x0F, 0x2E, 0x0F, 0x2C, 0x0F, 0x2C, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xE9, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x2A, 0x0F, 0x29, 0x10, 0x28, 0x0F, 0x25, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xF3, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x0C, 0x00, 0x02, 0x00, 0x3C, 0x00, 0x62, 0x0A, 0x28, // 20
	0xA5, 0x12, 5, 0x01, 0xFC, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x0E, 0x01, 0x1A, 0x00, 0x43, 0x00, 0xD9, 0x00, 0x00, // 20
	
	// case 1 -> 1 hb
	0xAA,
	byte(5 + 3), 0,
	2, 0, 0, 0, // 4
	0x01,             // 1 one A5s
	0xA5, 0x01, 0x04, // 0x04=hb
	
	// case 2 -> 4 cans
	0xAA,
	byte(5 + 4*20), 0, // 85,0
	3, 0, 0, 0, // 4
	0x04, // follow four A5s
	0xA5, 0x12, 5, 0x01, 0x58, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x21, 0x0F, 0x1B, 0x10, 0x11, 0x0F, 0x0D, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xB3, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0F, 0x0F, 0x0E, 0x0F, 0x0D, 0x0F, 0x0E, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xC1, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0E, 0x0F, 0x0D, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, // 20
	0xA5, 0x12, 5, 0x01, 0xCB, 0xFF, 0xFF, 0xFF, 0x10, 0x00, 0x01, 0x04, 0x0E, 0x10, 0x0F, 0x0F, 0x0E, 0x0F, 0x0D, 0x0F, // 20
}


func TestTcpRead(t *testing.T) {

	tt := base.Test{ T:t }

	// Keep test running 3 minutes
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer cancel()
	ctx, stop := signal.NotifyContext(ctx, os.Interrupt)
	defer stop()

	cfg := TcpCfg{
		Port:      2020,
		Frame:     *base.NewTime(100 * time.Millisecond),
		Heartbeat: *base.NewTime(3 * time.Second),
		Log:       1,
	}
	reqs := NewFrameReqs()
	writer := NewWriter(ctx, cfg.Frame.Duration)
	reader := NewReader(reqs)
	err := tcpClients(writer, reader, cfg)

	tt.Ok(err)
	written, done := make(chan bool, 1), make(chan bool, 1)
	go func() {
		for {
			select {
			case req := <- reqs.Req():
				if f := req.Frames; f != nil {
					t.Logf("frames: %d", len(f.Frames))
					if err := f.Err; err != nil {
						t.Logf("%sframe error: %v%s", base.Red, err, base.Reset)
					}
				}
				if req.Heartbeat {
					t.Logf("hb")
				}
				if can := req.Can; can != nil {
					t.Logf("can %v", can)
				}
			case <-written:
				done<- true
			}
		}
	}()
	go func() {
		time.Sleep(2 * time.Second)
		conn, err := net.Dial("tcp", "localhost:2020")
		tt.Ok(err)

		_, err = conn.Write(T1)
		tt.Ok(err)
		time.Sleep(1 * time.Second)
		written<- true
	}()
	<-done
}

