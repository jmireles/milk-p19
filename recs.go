package p19

import (
	"fmt"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/line"
)

type RecsCfg struct {
	Save base.Time `json:"save"`
	Log  int       `json:"log"`
}

func (r *RecsCfg) Validate() error {
	if r.Save.LowerThan(5 * time.Second) {
		return fmt.Errorf("save: time too small (< 5s)")
	}
	return nil
}

func RecsCfgDefault() RecsCfg {
	return RecsCfg{
		Save: base.Time{ 5 * time.Second },
		Log:  2,
	}
}

// Recs is used to test recordings in temporary folder
type Recs struct {
	cfg  RecsCfg
	recs *line.Recs
	log  base.Log
	hbs  int
}

func NewRecsTmp(cfg RecsCfg, tmp string) *Recs {
	var log base.Log
	if cfg.Log > 0 {
		log = func(f string, args ...any) {
			fmt.Print("Recs: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
	}
	recs := &Recs{
		cfg:  cfg,
		recs: line.NewRecs("", tmp, 0777, line.YMD_NLRI),
		log:  log,
	}
	recs.appendPC(true)
	return recs
}

func (r *Recs) IncrHB() {
	r.hbs++
}

func (r *Recs) appendPC(start bool) *line.Record {
	now, _ := base.UTCNow()
	var record *line.Record
	if start {
		record = line.NewRecordBox8(0, 0, []byte{1}, now)
	} else {
		record = line.NewRecordBox8(0, 0, []byte{2}, now)
	}
	if r.cfg.Log > 0 {
		r.logAppend("PC", record, nil)
	}
	r.recs.Append(record)
	return record
}

func (r *Recs) AppendMess(mess *line.Mess) {
	record, err := line.NewRecordMess(mess)
	if r.cfg.Log > 1 {
		s := fmt.Sprintf("line %02x hbs=%d", mess.Data, r.hbs)
		r.logAppend(s, record, err)
	}
	if err == nil {
		r.recs.Append(record)
	}
}

// Save saves zero or more records appended to buffer
func (r *Recs) Save() {
	saved, err := r.recs.Save()
	if r.cfg.Log > 0 {
		if err != nil {
			r.log("%sSave error: %v%s", base.Yellow, err, base.Reset)
		} else if len(saved) > 0 {
			r.log("%sSave: %v%s", base.Blue, saved, base.Reset)
		}
	}
}

func (r *Recs) logAppend(mess string, rec *line.Record, err error) {
	if err != nil {
		r.log("%sAppend skip %v mess=%s%s", base.Yellow, err, mess, base.Reset)
	} else {
		r.log("%sAppend ok %s%s %s%s", base.Green, rec.NLRI(), rec.Date(), rec.RowDebug(), base.Reset)	
	}
}

func (r *Recs) End(zip bool) error {
	record := r.appendPC(false) // save app stopped
	r.Save() // save everything pending

	var logFS func(string, int64)
	if r.cfg.Log > 0 {
		logFS = func(name string, size int64) {
			r.log("%sFile %v %v%s", base.Blue, name, size, base.Reset)
		}
	}
	// print all folder/files
	err := r.recs.Files(logFS)
	if err != nil {
		return err
	}
	if zip {
		// zip last-date folder
		err = r.recs.Zip(record.Date(), true, func(f string, args ...any) {
			r.log(f, args...)
		})
		if err != nil {
			return err
		}
		// print folder zipped
		return r.recs.Files(logFS)
	} else {
		return nil
	}
}
