package p19

import (
	"time"

	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/from"
)

// NowUTCMsec return milliseconds since 1970 UTC
func NowUTCMsec() int64 {
	nano := time.Now().UnixNano()
	return nano / 1_000_000
}

type Can struct {
	Net  byte
	Msec int64
	Data milk.Data
}

func NewCan(net byte, bytes []byte) *Can {
	data, err := milk.NewData(bytes)
	if err != nil {
		return nil 
	} else {
		return &Can{
			Net:  net,
			Msec: NowUTCMsec(), 
			Data: data,
		}
	}
}

func NewCanResp(resp from.Resp) *Can {
	utc := resp.UTC()
	y, m, d := 2000 + int(utc[0]), time.Month(utc[1]), int(utc[2])
	h, min, s := int(utc[3]), int(utc[4]), int(utc[5])
	cents := utc[6]
	nano := 10_000_000 * int(cents)
	t := time.Date(y, m, d, h, min, s, nano, time.UTC)
	return &Can{
		Net:  byte(resp.NetId()),
		Msec: t.UnixNano() / 1_000_000,
		Data: resp.Bytes(),
	}
}

func NewCanMess(mess *line.Mess) *Can {
	utc := mess.UTC()
	y, m, d := 2000 + int(utc[0]), time.Month(utc[1]), int(utc[2])
	h, min, s := int(utc[3]), int(utc[4]), int(utc[5])
	cents := utc[6]
	nano := 10_000_000 * int(cents)
	t := time.Date(y, m, d, h, min, s, nano, time.UTC)
	return &Can{
		Net:  byte(mess.Net),
		Msec: t.UnixNano() / 1_000_000,
		Data: mess.Data,
	}
}


