package p19

import (
	"fmt"
	"time"
)

type Frame []byte

type Frames struct {
	Fit    Fit
	Time   int64
	Frames []Frame
	Err    error
}

func FrameBytes(A5s [][]byte) ([]byte, error) {
	size := 8 // set 0xAA(1), size(2), time(4), countA5(1)
	for _, a5 := range A5s {
		size += 2       // set len(a5), 0xA5
		size += len(a5) // set all a5
	}
	if (size - 3) > 0xFFFF {
		return nil, fmt.Errorf("Frame.Size-3 > 0xFFFF")
	}
	out := make([]byte, size)
	out[0] = 0xAA
	out[1] = byte((size - 3))
	out[2] = byte((size - 3) >> 8)

	ms := time.Now().Unix()
	out[3] = byte(ms)
	out[4] = byte(ms >> 8)
	out[5] = byte(ms >> 16)
	out[6] = byte(ms >> 24)

	if len(A5s) > 255 {
		return nil, fmt.Errorf("Frame.len(A5s) > 255")
	}
	out[7] = byte(len(A5s))
	pos := 8
	for i, a5 := range A5s {
		if len(a5) > 255 {
			return nil, fmt.Errorf("Frame.255 < len(A5):%d", i)
		}
		out[pos] = 0xA5
		pos++
		out[pos] = byte(len(a5))
		pos++
		for _, a := range a5 {
			out[pos] = a
			pos++
		}
	}
	return out, nil
}

type FrameReq struct {
	Frames    *Frames
	Can       *Can
	Resp      []byte // status, heartbeat
	Heartbeat bool      
}

type FrameReqs struct {
	req       chan *FrameReq
	status    *FrameReq
	heartbeat *FrameReq
}

func NewFrameReqs() *FrameReqs {
	return &FrameReqs{
		req: make(chan *FrameReq),
		status: &FrameReq{
			Resp: writerRespStatus(),
		},
		heartbeat: &FrameReq{
			Heartbeat: true,
		},
	}
}

func (c *FrameReqs) Req() <-chan *FrameReq {
	return c.req
}

func (c *FrameReqs) fireFrames(frames *Frames) {
	c.req<- &FrameReq{
		Frames: frames,
	}
}

func (c *FrameReqs) fireCan(can *Can) {
	c.req<- &FrameReq{
		Can: can,
	}
}

func (c *FrameReqs) fireStatus() {
	// send constant status message
	c.req<- c.status
}

func (c *FrameReqs) fireHeartbeat() {
	// send constant heartbeat message
	c.req<- c.heartbeat
}





