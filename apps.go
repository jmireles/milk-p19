package p19

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/pub"
)

type Apps struct {
	Tcp  TcpCfg      `json:"tcp,omitempty"`
	Srvs SrvsCfg     `json:"services,omitempty"`
	Recs RecsCfg     `json:"records,omitempty"`
	Mqtt pub.MqttCfg `json:"mqtt,omitempty"`

	wg sync.WaitGroup
}

func AppsDefault() Apps {
	return Apps {
		Tcp:  TcpCfgDefault(),
		Srvs: SrvsCfgDefault(),
		Recs: RecsCfgDefault(),
	}
}

func (c *Apps) Validate() error {
	if err := c.Tcp.Validate(); err != nil {
		return fmt.Errorf("json tcp.%v", err)
	}
	if err := c.Srvs.Validate(); err != nil {
		return fmt.Errorf("json services.%v", err)
	}
	if err := c.Recs.Validate(); err != nil {
		return fmt.Errorf("json records.%v", err)
	}
	return nil
}

// Process starts
func (a *Apps) Start(ctx context.Context, recs *Recs, mess *line.Messager) error {

	p19s, err := NewTcp(ctx, a.Tcp)
	if err != nil {
		return err
	}
	var mqtt *line.Mqtt
	if a.Mqtt.Port > 0 {
		mqtt, err = line.NewMqtt(ctx, a.Mqtt)
		if err != nil {
			return err
		}
	}

	a.wg.Add(1)
	go func() {
		defer a.wg.Done()
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		ticker := time.NewTicker(a.Recs.Save.Duration)
		defer ticker.Stop()
		for {
			select {

			case <- ticker.C:
				recs.Save()

			case resp := <- mess.Resp():
				p19s.Resp(resp)
				recs.AppendMess(resp)
				if mqtt != nil {
					mqtt.Mess(resp)
				}

			case req := <- p19s.Req:
				if req.Heartbeat {
					recs.IncrHB()
				}
				if can := req.Can; can != nil {
					if now, err := base.UTCNow(); err != nil {
						// silent error?
					} else if req, err := line.NewMess(can.Net, can.Data, now); err != nil {
						// silent error?
					} else {
						recs.AppendMess(req)
						go mess.Req(req)
					}
				}

			case <-ctx.Done():
				return
			}
		}
	}()
	return nil
}

func (a *Apps) Services(ctx context.Context) {
	a.wg.Add(1)
	go func() {
		defer a.wg.Done()
		a.Srvs.Start(ctx)
	}()
}

func (a *Apps) Wait() {
	a.wg.Wait()
	time.Sleep(1 * time.Second)
}

