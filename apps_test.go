package p19

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/line/t4"
	"gitlab.com/jmireles/cans-base/milk/pub"
)

const testCfg = `{
	"tcp":{
		"port":      2020,
		"frame":     "100ms",
		"heartbeat": "3s",
		"log":       1
	},
	"services":{
		"home":    true,
		"folder":  "rt2019",
		"restart": "1m",
		"health":  "20s",
		"url":     "http://localhost:8080",
		"client":  "5s",
		"log":     2
	},
	"records":{
		"save": "5s",
		"log":  2
	}
}`

func TestAppsCfg(t *testing.T) {
	tt := &base.Test{ T:t }
	var apps Apps
	tt.Ok(json.Unmarshal([]byte(testCfg), &apps))
	tt.Equals(AppsDefault(), apps)

}

func TestAppsP1(t *testing.T) {

	tt := &base.Test{ T:t }
	var apps Apps
	tt.Ok(json.Unmarshal([]byte(testCfg), &apps))
	tt.Ok(apps.Validate())

	// Keep test running 3 minutes at most
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer cancel()
	ctx, stop := signal.NotifyContext(ctx, os.Interrupt)
	defer stop()

	// recs saves records in tmp folder
	recs := NewRecsTmp(apps.Recs, t.TempDir())	
	defer func() {
		zip := true
		tt.Ok(recs.End(zip))
	}()
	// sim simulates equipment
	net := byte(1)
	mess, unsol := line.NewSims(ctx, net, map[string]*line.Sim {
		"010203": line.NewSim(from.P1, 11, 1),
		"010204": line.NewSim(from.P1, 12, 2),
		"010205": line.NewSim(from.P1, 13, 3),
	})
	_ = unsol
	apps.Start(ctx, recs, mess)
	apps.Services(ctx)
	apps.Wait()
}

func TestAppsT4(t *testing.T) {
	tt := &base.Test{ T:t }
	apps := AppsDefault()
	apps.Mqtt = pub.MqttCfg{
    	Port:     1883,
		//Username: "user",
		//Password: "password",
		Pubs:     []string { "t4" }, 
	}

	// Keep test running 3 minutes at most
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer cancel()
	ctx, stop := signal.NotifyContext(ctx, os.Interrupt)
	defer stop()

	// recs saves records in tmp folder
	recs := NewRecsTmp(apps.Recs, t.TempDir())	
	defer func() {
		zip := true
		tt.Ok(recs.End(zip))
	}()
	// sim simulates equipment
	net := byte(1)
	mess, unsol := line.NewSims(ctx, net, map[string]*line.Sim {
		"010203": line.NewSim(from.T4, 11, 1),
		"010204": line.NewSim(from.T4, 12, 2),
		"010205": line.NewSim(from.T4, 13, 3),
	})

	apps.Start(ctx, recs, mess)

	// mqtt client
	log := func(f string, args ...any) {
		fmt.Printf("Mqtt: Client " + f + "\n", args...)
	}
	err := pub.MqttClient(ctx, pub.MqttCfg{
		Port:     1883,
		//Username: "user",
		//Password: "password",
    	Broker:   "127.0.0.1",
		Topics:   []string{ "stall/+/t4" },
	}, nil, log)
	tt.Ok(err)

	// simulate 5 t4s stalls send starts/stops
	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		ticker := time.NewTicker(1 * time.Second)
		defer ticker.Stop()
		nextUnsol := testNextUnsolT4()
		for {
			select {
			case <-ticker.C:
				unsol<- nextUnsol()

			case <-ctx.Done():
				return
			}
		}
	}()
	
	apps.Services(ctx)

	apps.Wait()

}

func testNextUnsolT4() func() []byte {
	pos := 0
	bytes := make([][]byte, 0)
	const start, stop = t4.StageLetDown, t4.StageReady
	for _, m := range []struct{
		box   byte
		extra []byte
	} {
		{ box:1, extra:[]byte{ 1, byte(start | 1), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 2, byte(start | 2), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 3, byte(start | 3), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 4, byte(start | 4), 0, 0, 0, 0 } },
		{ box:2, extra:[]byte{ 5, byte(start | 1), 0, 0, 0, 0 } },
		{ box:2, extra:[]byte{ 5, byte(stop  | 1), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 4, byte(stop  | 4), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 3, byte(stop  | 3), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 2, byte(stop  | 2), 0, 0, 0, 0 } },
		{ box:1, extra:[]byte{ 1, byte(stop  | 1), 0, 0, 0, 0 } },
	} {
		bytes = append(bytes, from.T4.RespUnsolBytes(m.box, t4.Unsol09_Stage, m.extra))
	}
	return func() []byte {
		b := pos % len(bytes)
		pos++
		return bytes[b]
	}
}


