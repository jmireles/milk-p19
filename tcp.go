package p19

import (
	"context"
	"fmt"
	"net"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/line"
)

type TcpCfg struct {
	Port      int       `json:"port"`     // default 2020
	Frame     base.Time `json:"frame"`    // default 100*time.Millisecond
	Heartbeat base.Time `json:"heartbeat` // default 3 * time.Second
	Log       int       `json:"log"`      // 
}

func TcpCfgDefault() TcpCfg {
	return  TcpCfg{
		Port: 2020,
		// send frames accumulated every 100ms
		Frame: base.Time{ 100*time.Millisecond },
		// send a heartbeat every 3s
		Heartbeat: base.Time{ 3*time.Second },
		// log clients add/delete events
		Log: 1,
	}
}

func (c *TcpCfg) Validate() error {
	if c.Port < 1024 || c.Port > 0xFFFF {
		return fmt.Errorf("port out of range: %d", c.Port)
	}
	if c.Frame.LowerThan(100*time.Millisecond) {
		return fmt.Errorf("frame: time too small (< 100ms)")
	}
	if c.Frame.GreaterThan(1*time.Second) {
		return fmt.Errorf("frame: time too large (> 1s)")
	}

	if c.Heartbeat.LowerThan(1 * time.Second) {
		return fmt.Errorf("heartbeat: time too small (< 1s)")
	}
	if c.Heartbeat.GreaterThan(5 * time.Second) {
		return fmt.Errorf("heartbeat: time too large (> 5s)")
	}
	return nil
}

type Tcp struct {
	writer *Writer
	Req    <-chan *FrameReq
}

func NewTcp(ctx context.Context, cfg TcpCfg) (*Tcp, error) {
	reqs := NewFrameReqs()
	writer := NewWriter(ctx, cfg.Frame.Duration)
	reader := NewReader(reqs)
	if err := tcpClients(writer, reader, cfg); err != nil {
		return nil, err
	}
	return &Tcp{
		writer: writer,
		Req:  reqs.Req(),
	}, nil
}

func (p *Tcp) Resp(mess *line.Mess) {
	can := NewCanMess(mess)
	p.writer.AppendCan(can)
}



func tcpClients(writer *Writer, reader *Reader, cfg TcpCfg) error {
	var log base.Log
	if cfg.Log > 0 {
		log = func(f string, args ...any) {
			fmt.Print("Tcps: ")
			fmt.Printf(f, args...)
			fmt.Println()
		}
	}
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.Port))
	if err != nil {
		return err
	}
	add    := make(chan *Client)
	remove := make(chan *Client)
	go func() {
		clients := make(map[*Client]bool)
		for {
			select {
			
			case client := <-add:
				clients[client] = true
				writer.Clients(clients)
				if log != nil {
					log("client added, clients=%d", len(clients))
				}
			
			case client := <-remove:
				if _, ok := clients[client]; ok {
					close(client.data)
					delete(clients, client)
				}
				writer.Clients(clients)
				if log != nil {
					log("client removed, clients=%d", len(clients))
				}
			}
		}
	}()
	go func() {
		for {
			conn, err := listener.Accept() // conn
			if err != nil {
				fmt.Println(err)
				return
			}
			client := newClient()
			add <- client
			
			go func() {
				// receiver
				a := make([]byte, 4096)
				for {
					if length, err := conn.Read(a); err != nil {
						remove <- client
						conn.Close()
						break
					} else if length > 0 {
						m := a[0:length]
						reader.Read(m)
					}
				}
			}()
			
			go func() {
				// sender
				ticker := time.NewTicker(cfg.Heartbeat.Duration)
				defer conn.Close()
				for {
					select {
					case mess, ok := <-client.data:
						if !ok {
							return
						}
						conn.Write(mess)
					
					case <-ticker.C:
						writer.AppendHeartbeat()
					}
				}
			}()
		}
	}()
	return nil
}


type Client struct {
	data chan []byte
}

func newClient() *Client {
	client := &Client{
		data: make(chan []byte, 1),
	}
	return client
}

func (c *Client) write(bytes []byte) {
	defer func() {
		// prevent write on closed channel
		if r := recover(); r != nil {
			return
		}
	}()
	c.data<- bytes
}



