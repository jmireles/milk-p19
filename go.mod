module gitlab.com/jmireles/milk-p19

go 1.19

require (
	gitlab.com/jmireles/cans-base v0.0.0-20230523184629-149573a4ec3b
	golang.org/x/net v0.10.0
	golang.org/x/text v0.9.0
)

require (
	github.com/eclipse/paho.mqtt.golang v1.4.2 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mochi-co/mqtt/v2 v2.2.9 // indirect
	github.com/rs/xid v1.4.0 // indirect
	github.com/rs/zerolog v1.28.0 // indirect
	golang.org/x/sync v0.0.0-20220929204114-8fcdb60fdcc0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
